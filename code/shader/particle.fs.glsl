/**
 * particle fragment shader
 */
precision mediump float;

// particle varying
varying float v_lifetime;
varying float v_render;
// particle texture
uniform sampler2D u_tex;
// particle color uniforms
uniform vec4 u_startColor;
uniform vec4 u_middleColor;
uniform vec4 u_endColor;

/*
 * Scale value into given range.
 */
float scale(float value, float min, float max) {
    return (value - min) / (max - min);
}

void main(void) {
    // don't render particle if vertex shader says so ..
    if(v_render < 0.5) {
        gl_FragColor = vec4(0,0,0,0);
        return;
    }
    // otherwise, get color from texture
    vec4 textureColor = texture2D(u_tex, gl_PointCoord);

    // set color ramp threshold
    float threshold = 0.9;
    if(v_lifetime > threshold) {
        // set color to interpolation between start and middle color
        gl_FragColor = mix(u_middleColor, u_startColor, scale(v_lifetime, threshold, 1.0));
    } else if(v_lifetime <= threshold) {
        // set color to interpolation between middle and end color
        gl_FragColor = mix(u_endColor, u_middleColor, scale(v_lifetime, 0.0, threshold));
    }
    // multiplicative blending with texture
    gl_FragColor *= textureColor;
    // additive ("Whiteness") correction -> reduce visibility on beginning
    if(v_lifetime >= 0.8) {
        gl_FragColor.a *= 0.2;
    } else {
        // set visibility to remaining lifetime
        gl_FragColor.a *= v_lifetime;
    }
}