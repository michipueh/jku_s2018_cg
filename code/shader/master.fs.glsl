precision highp float;

/**
 * definition of a material structure containing common properties
 */
struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
	float reflectiveness;
};

/**
 * definition of the light properties related to material properties
 */
struct Light {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

// MATERIAL
// material of the fragment
uniform Material u_material;

// POINTLIGHT
// point light of the scene
uniform Light u_pointLight;
// enable for light
uniform bool u_enableLight;
// direction of the point light to the surface point
varying vec3 v_pointLightVec;

// SPOTLIGHT
// spotlight of the scene
uniform Light u_spotLight;
// enable for spotlight
uniform bool u_enableSpotlight;
// direction of the spotlight to the surface point
varying vec3 v_spotLightVec;
// direction of the spotlight
uniform vec3 u_spotLightDirection;
// limit (boundary) of the spotlight
uniform float u_spotLightLimit;

// TEXTURES
// texture coordinate of the vertex
varying vec2 v_texCoord;
// texture sampler (if used)
uniform sampler2D u_tex;
// enable for texture
uniform bool u_enableTexture;

// alpha sampler (if used)
uniform sampler2D u_alpha;
// enable for alpha texture
uniform bool u_enableAlpha;

// alpha texture sampler (if used)
uniform sampler2D u_alphaBlending;
// enable for alpha blending texture
uniform bool u_enableAlphaBlending;

// cubemap sampler for skymap (if used)
uniform samplerCube u_texCube;
// enable for skymap texture
uniform bool u_enableSkymap;
// enabled if skymap sphere is rendered currently (different reflections)
uniform bool u_isSkymapSphere;
// direction vector required for reflection calculation
varying vec3 v_cameraRayVec;

//varying vectors for light computation
// direction of the normal vector
varying vec3 v_normalVec;
// inverse direction of the normal vector (for skymap calculations)
varying vec3 v_normalVecInv;
// direction of the eye to the surface point
varying vec3 v_eyeVec;

/**
 * Computes the color of a vertex given its material and the surrounding light.
 */
vec4 calculatePhongColor(Light light,
        Material material,
        vec3 lightVec,
        vec3 normalVec,
        vec3 eyeVec,
        vec4 textureColor) {
    // normalize all direction vectors
	lightVec = normalize(lightVec);
	normalVec = normalize(normalVec);
	eyeVec = normalize(eyeVec);

	// compute diffuse term
	float diffuse = max(dot(normalVec, lightVec), 0.0);

	//compute specular term
	vec3 reflectVec = reflect(-lightVec, normalVec);
	float spec = pow(max(dot(reflectVec, eyeVec), 0.0), material.shininess);

    // if a texture is enabled and is currently visible
    // needed because the material should be visible "behind" a texture with alpha properties.
	if(u_enableTexture) {
	    // use additive blending to mix both colors according to alpha value of the texture.
	    material.diffuse.rgb = (textureColor * textureColor.a + material.diffuse * (1.0 - textureColor.a)).rgb;
	    material.ambient.rgb = (textureColor * textureColor.a + material.ambient * (1.0 - textureColor.a)).rgb;
	}

	//use term an light to compute the components
	vec4 c_amb  = clamp(light.ambient * material.ambient, 0.0, 1.0);
	vec4 c_diff = clamp(diffuse * light.diffuse * material.diffuse, 0.0, 1.0);
	vec4 c_spec = clamp(spec * light.specular * material.specular, 0.0, 1.0);
	vec4 c_em   = material.emission;

    // return colors (with respect to material reflectiveness)
	return (c_amb + c_diff + c_spec + c_em) * (1.0 - material.reflectiveness);
}

void main() {
    vec3 normalVec = normalize(v_normalVec);
    vec3 normalVecInv = normalize(v_normalVecInv);
    vec3 cameraRayVec = normalize(v_cameraRayVec);

    // calculate texture (if enabled)
    vec4 textureColor = vec4(0,0,0,1);
    if(u_enableTexture) {
        textureColor =  texture2D(u_tex, v_texCoord);
    }

    // calculate pointlight
    if(u_enableLight) {
        gl_FragColor = calculatePhongColor(
            u_pointLight,
            u_material,
            v_pointLightVec,
            v_normalVec,
            v_eyeVec,
            textureColor);
    } else {
        gl_FragColor = vec4(0, 0, 0, 1);
    }

    // calculate spotlight
    if(u_enableSpotlight) {
        // normalize direction vectors
        vec3 spotLightVec = normalize(v_spotLightVec);
        vec3 spotLightDir = normalize(u_spotLightDirection);
        // calculate dot-product between eye to surface and light vector
        float dotFromDirection = dot(spotLightVec, -spotLightDir);
        // if dot-product is greater than limit --> fragment is within limit
        if(dotFromDirection >= u_spotLightLimit) {
            // calculate light intensity
            float light = dot(normalVec, spotLightVec);
            // if fragment is in light ..
            if(light > 0.0) {
                // add color (with respecet to light intensity)
                gl_FragColor += calculatePhongColor(
                    u_spotLight,
                    u_material,
                    v_spotLightVec,
                    v_normalVec,
                    v_eyeVec,
                    textureColor) * light;
            }
        }
    }

    // process skymap and reflections (if enabled)
    if(u_enableSkymap) {
        vec3 texCoords;
        // if it is the skymap sphere ..
        if(u_isSkymapSphere) {
            // .. use direct camera direction as texture coordinates
            texCoords = cameraRayVec;
        } else {
            // use reflected camera direction as texture coordinates
            texCoords = reflect(cameraRayVec, normalVecInv);
        }
        // add color (with respect to reflectiveness of material) to global color
        gl_FragColor += textureCube(u_texCube, texCoords) * u_material.reflectiveness;
    }

    // calculate alpha map (if enabled)
    if(u_enableAlpha) {
        // get alpha color from texture (is black/white)
        vec4 alphaColor = texture2D(u_alpha, v_texCoord);
        // convert to greyscale value
        float greyscaleValue =  0.299 * alphaColor.r + 0.587 * alphaColor.g + 0.114 * alphaColor.b;
        // if alpha blending is enabled
        if(u_enableAlphaBlending) {
            // get color from alpha blending map
            vec4 alphaBlendingColor = texture2D(u_alphaBlending, v_texCoord);
            // use additive color adding
            gl_FragColor = alphaBlendingColor * greyscaleValue +
                gl_FragColor * (1.0 - greyscaleValue);
        } else {
            // alpha value is grey value
            gl_FragColor.a = greyscaleValue;
        }
    }
}