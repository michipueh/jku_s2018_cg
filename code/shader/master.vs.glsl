// position and normal of vertex
attribute vec3 a_position;
attribute vec3 a_normal;
// general scene (pipeline) matrizes
uniform mat4 u_modelView;
uniform mat3 u_normalMatrix;
uniform mat4 u_projection;

// pointlight uniforms
uniform bool u_enableLight;
uniform vec3 u_pointLightPos;
// spotlight uniforms
uniform bool u_enableSpotlight;
uniform vec3 u_spotLightPos;

// given texture coordinates per vertex
attribute vec2 a_texCoord;
varying vec2 v_texCoord;
// texture map uniform
uniform sampler2D u_tex;
// displacement map uniforms
uniform bool u_enableDisplacement;
uniform sampler2D u_dis;
uniform float u_displacementFactor;
// normal map uniforms
uniform bool u_enableNormal;
uniform sampler2D u_normal;

// varyings
varying vec3 v_normalVec;
varying vec3 v_normalVecInv;
varying vec3 v_eyeVec;
// vector from light to eye
varying vec3 v_pointLightVec;
varying vec3 v_spotLightVec;

// enables
uniform bool u_isSkymapSphere;

// if reflection is used
// inverse view matrix to get from eye to world space
uniform mat3 u_invView;
varying vec3 v_cameraRayVec;

void main() {
	vec3 normal = a_normal;
	float displace = 0.0;
    // add displacement from texture (if enabled)
	if(u_enableDisplacement) {
	    // get texture color from displacement map
        vec4 textureColor = texture2D(u_dis, a_texCoord);
        // calculate greyscale value of the color (from 0 to 1)
	    displace = 0.299 * textureColor.x + 0.587 * textureColor.y + 0.114 * textureColor.z;
	    // normalize displacement to -0.5 and 0.5
	    displace -= 0.5;
	    // multiply greyscale value by displacement factor
	    displace *= u_displacementFactor;
	}
	// calculate vertex position
	vec3 displacedPosition = a_position + normal * displace;

    // calculate vertex position in eye space
	vec4 eyePosition = u_modelView * vec4(displacedPosition,1);

    // use normal vector from texture (if enabled)
	if(u_enableNormal) {
	    normal = texture2D(u_normal, a_texCoord).xyz;
	}

    // pass light to surface vector as varying (if enabled)
    if(u_enableLight) {
        v_pointLightVec = u_pointLightPos - eyePosition.xyz;
    }
    // pass spotlight to surface vector as varying (if enabled)
    if(u_enableSpotlight) {
        v_spotLightVec = u_spotLightPos - (u_normalMatrix * displacedPosition);
    }

    // pass normal vector as varying
    v_normalVec = u_normalMatrix * normal;
    v_normalVecInv = u_invView * u_normalMatrix * normal;
    // pass texture coordinate as varying
    v_texCoord = a_texCoord;
    // pass eye direction vector as varying
    v_eyeVec = -eyePosition.xyz;
    // pass camera ray vector (for skymap lookup) as varying
	v_cameraRayVec = u_invView * eyePosition.xyz;
    // set vertex position (with optional displacement)
	gl_Position = u_projection * eyePosition;
}
