 /**
  * particle vertex shader
  */

// scene uniforms
uniform mat4 u_modelView;
uniform mat4 u_projection;
// particle attributes
attribute float a_lifetime;
attribute float a_timeOffset;
attribute vec3 a_start;
attribute vec3 a_end;
// particle varyings
varying float v_lifetime;
varying float v_render;
// particle uniforms
uniform float u_pointSize;
uniform float u_time;
uniform bool u_started;
uniform vec3 u_offset;
uniform bool u_repeat;

void main(void) {
    float time = u_time;
    // if it is not started yet or if repeat is disabled (and particle is dead)
    if(!u_started || time < a_timeOffset || (!u_repeat && time > a_lifetime)) {
        // set time to 0 and do not render it in fragment shader.
        time = 0.0;
        v_render = 0.0;
    } else {
        // otherwise set correct time (-offset) and render it in fragment shader.
        time -= a_timeOffset;
        v_render = 1.0;
    }
    // if time exceeded lifetime, modulo it to get a time between 0..lifetime again
    time = mod(time, a_lifetime);
    // set lifetime varying (1.0 ... New, 0.0 ... Old)
    v_lifetime = 1.0 - (time / a_lifetime);
    v_lifetime = clamp(v_lifetime, 0.0, 1.0);
    // linear interpolation (1.0 ... a_start, 0.0 ... a_end)
    gl_Position.xyz = mix(a_end, a_start, v_lifetime);
    // add offset
    gl_Position.xyz += u_offset;
    // set 4th coordinate to 1.0
    gl_Position.a = 1.0;
    // calculate position in scene
    gl_Position = u_projection * u_modelView * gl_Position;
    // calculate point size from varying in quadratic manner
    gl_PointSize = (v_lifetime * v_lifetime) * u_pointSize;
}