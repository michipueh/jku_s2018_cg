//the OpenGL context
let gl = null,
    program = null,
    particleProgram = null;

// canvas settings
let canvasWidth = 800;
let canvasHeight = 800;
let aspectRatio = canvasWidth / canvasHeight;

// rendering context
let context;

// important nodes
let rootNode;
let particleNode;
let pointLightNode;
let spotLightNode;

// time variable
let timeInS = 0.0;
let lastShot = 0.0;

// global positions
let attackerHeadPosition = vec3.fromValues(0, 0, 0);
let defenderHeadPosition = vec3.fromValues(0, 0, 0);
let attackerCannonRightPosition = vec3.fromValues(0, 0, 0);
let attackerCannonLeftPosition = vec3.fromValues(0, 0, 0);


// UI control variables
let debug = false;
let timeOffset = 0.0;

let cannonRightNode = new AnimationSGNode(undefined, (matrix) => {
    AnimationSGNode.translate(matrix, attackerCannonRightPosition);
});
let cannonLeftNode = new AnimationSGNode(undefined, (matrix) => {
    AnimationSGNode.translate(matrix, attackerCannonLeftPosition);
});
let cannonNode = createMaterial({
    //emission: [1, 0.0, 0, 1],
    ambient: [0.25, 0.25, 0.25, 1.0],
    diffuse: [0.4, 0.4, 0.4, 1.0],
    specular: [0.775, 0.775, 0.775, 1.0],
    shininess: 0.768
}, [cannonRightNode, cannonLeftNode]);

let mouseControl, keyboardControl;

// Camera (Animation) variables
let eyeAnimation = vec3.fromValues(-5, 0, 0);
let centerAnimation = vec3.fromValues(-5, 0, 0);
let upAnimation = vec3.fromValues(0, 1, 0);
// Camera (Manual) variables
let eye = vec3.fromValues(0, 0, 0);
let up = vec3.fromValues(0, 1, 0);
let centerPivot = vec3.fromValues(1, 0, 0);
// indicator if camera is following animations (1) or manual (0)
let followCamera = true;
// currently active scene (0 .. 1. Scene, 1 .. 2. Scene, 2 .. 3. Scene, -1 .. No active scene)
let activeScene = 0;
// offsets of the three scenes (along z axis)
let sceneOffsets = [-50, 0, 50];

/**
 * initializes OpenGL context, compile shader, and load buffers
 */
function init(res) {
    //create a GL context
    gl = createContext(canvasWidth, canvasHeight);

    // init keyboard and mouse
    keyboardControl = new KeyboardControl(
        keyCode => {
            // if C was pressed
            if (keyCode === 67) {
                changeViewMode();
            }
        }
    );
    mouseControl = new MouseControl(gl.canvas);

    loadDefaults();

    // start keyboard and mouse inputs
    keyboardControl.start();
    mouseControl.start();

    //compile and link shader program
    program = createProgram(gl, res.master_vs, res.master_fs);
    particleProgram = createProgram(gl, res.particle_vs, res.particle_fs)

    //region Nodes
    rootNode = new ShaderSGNode(program);
    particleNode = new ShaderSGNode(particleProgram);

    // Scene dependent nodes
    //region Camera Eye Node
    const cameraEyeNode = new AnimationSGNode(
        glm.translate(0, 0, 0),
        (matrix) => {
            let myTime = timeInS / 10;
            if (activeScene === -1) {
                myTime = 0.0;
            } else if (activeScene === 0) {
                myTime %= 1.0;
                let point = AnimationSGNode.bezier4(myTime,
                    [10, 0.5, -6],
                    [3, 0.5, -1],
                    [-3, 0.5, -1],
                    [-3, 0.5, -4]);
                AnimationSGNode.translate(matrix, point);
            } else if (activeScene === 1) {
                myTime -= 1;
                myTime %= 1.5;
                if (myTime < 1) {
                    let point = AnimationSGNode.bezier4(myTime,
                        [-14, 5, -3],
                        [-12, 2, -3],
                        [-8, 1, -3],
                        [-2.8, 1.3, -2.5]);
                    AnimationSGNode.translate(matrix, point);
                } else {
                    AnimationSGNode.translate(matrix, attackerHeadPosition);
                    AnimationSGNode.translate(matrix, [0.2, 0, 0]);
                }
            } else if (activeScene === 2) {
                myTime -= 2.5;
                myTime %= 0.5;
                let point = AnimationSGNode.bezier2(myTime * 2,
                    [8.5, 1, -3],
                    [5, 1.2, -2.8]);
                AnimationSGNode.translate(matrix, point);
            }
            if (activeScene >= 0) {
                AnimationSGNode.translate(matrix, [0, 0, sceneOffsets[activeScene]]);
            }
        }, new RenderSGNode(
            () => {
                eyeAnimation = [
                    context.sceneMatrix[12],
                    context.sceneMatrix[13],
                    context.sceneMatrix[14]
                ];
            }, (debug) ?
                createMaterial({emission: [1, 0, 0, 0.2]},
                    new TransformationSGNode(
                        glm.translate(0, 1, 0),
                        new TransformationSGNode(
                            glm.scale(0.15, 0.15, 0.15), new CubeRenderNode())))
                : []
        ));
    //endregion
    //region Camera Center Node
    const cameraCenterNode = new AnimationSGNode(
        glm.translate(0, 0, 0),
        (matrix) => {
            let myTime = timeInS / 10;
            if (activeScene === -1) {
                myTime = 0.0;
            } else if (activeScene === 0) {
                myTime %= 1.0;
                let point = AnimationSGNode.bezier4(myTime,
                    [7, 0.5, -6],
                    [3, 0.5, -1],
                    [-3, 0.5, -1],
                    [0, 0.5, -4]);
                AnimationSGNode.translate(matrix, point);
            } else if (activeScene === 1) {
                myTime -= 1.0;
                myTime %= 1.5;

                let point = AnimationSGNode.bezier4(myTime,
                    [-12, 4, -3],
                    [-10, 1, -3],
                    [-5, 1, -3],
                    [3, 0.8, -3.0]);
                AnimationSGNode.translate(matrix, point);
            } else if (activeScene === 2) {
                myTime -= 2.50;
                myTime %= 0.5;

                let point = AnimationSGNode.bezier2(myTime * 2,
                    [3, 0, -3.5],
                    [3, 1, -2.5]);
                AnimationSGNode.translate(matrix, point);
            }
            if (activeScene >= 0) {
                AnimationSGNode.translate(matrix, [0, 0, sceneOffsets[activeScene]]);
            }
        }, new RenderSGNode(
            () => {
                centerAnimation = [
                    context.sceneMatrix[12],
                    context.sceneMatrix[13],
                    context.sceneMatrix[14]
                ];
            }, (debug) ?
                createMaterial({emission: [0, 0, 1, 0.2]},
                    new TransformationSGNode(
                        glm.scale(0.1, 0.1, 0.1), new CubeRenderNode()))
                : []
        ));
    //endregion
    //region Spaceship Attacker
    const attackerNode =
        new AnimationSGNode(
            glm.translate(0, 0, 0),
            (matrix) => {
                let myTime = timeInS / 10;
                if (activeScene === -1) {
                    myTime = 0.0;
                } else if (activeScene === 0) {
                    myTime %= 1.0;
                    let point = AnimationSGNode.bezier4(myTime,
                        [-13, 0, -8],
                        [-6, 0, 0],
                        [0, 0, -2],
                        [7, 0, -4])
                    ;
                    AnimationSGNode.translate(matrix, point);
                    let rotation = AnimationSGNode.bezier2(myTime,
                        [0, -40, 0],
                        [0, 40, 0])
                    ;
                    AnimationSGNode.rotate(matrix, rotation);
                } else if (activeScene === 1 || activeScene === 2) {
                    myTime -= 1;
                    myTime %= 2;
                    if (activeScene === 2) {
                        myTime %= 0.5;
                        myTime += 1.5;
                    }
                    AnimationSGNode.translate(matrix, [myTime * 10 - 13, 0, -3]);
                }
                AnimationSGNode.translate(matrix,
                    [0,
                        Math.cos(timeInS * Math.PI / 2) / 2,
                        Math.cos(timeInS * Math.PI / 2) / 2]);
                AnimationSGNode.rotate(matrix, [
                    Math.sin(timeInS * Math.PI * 0.5) * 15, 0, 0]);
            },
            new RenderSGNode(
                () => {
                }, createSpaceship(res, res.rebels_triangle, {
                        head: new RenderSGNode(
                            () => {
                                attackerHeadPosition = [
                                    context.sceneMatrix[12],
                                    context.sceneMatrix[13],
                                    context.sceneMatrix[14]];
                            }),
                        cannonRight: new RenderSGNode(
                            () => {
                                attackerCannonRightPosition = [
                                    context.sceneMatrix[12],
                                    context.sceneMatrix[13],
                                    context.sceneMatrix[14]];
                            }),
                        cannonLeft: new RenderSGNode(
                            () => {
                                attackerCannonLeftPosition = [
                                    context.sceneMatrix[12],
                                    context.sceneMatrix[13],
                                    context.sceneMatrix[14]];
                            }),
                    }
                )));
    //endregion
    //region Spaceship Defender
    const defenderNode =
        new AnimationSGNode(
            glm.translate(0, 0, 0),
            (matrix) => {
                let myTime = timeInS / 10;
                if (activeScene === -1) {
                    myTime = 0.0;
                } else if (activeScene === 0) {
                    myTime %= 1.0;
                    let point = AnimationSGNode.bezier4(myTime,
                        [-10, 0, -7],
                        [-3, 0, 0],
                        [3, 0, 0],
                        [10, 0, -7]);
                    AnimationSGNode.translate(matrix, point);
                    let rotation = AnimationSGNode.bezier2(myTime,
                        [0, -40, 0],
                        [0, 40, 0]);
                    AnimationSGNode.rotate(matrix, rotation);
                } else if (activeScene === 1 || activeScene === 2) {
                    myTime -= 1;
                    myTime %= 2;
                    AnimationSGNode.translate(matrix, [myTime * 10 - 7, 0, -3]);
                    if (myTime >= 1) {
                        myTime -= 1;
                        AnimationSGNode.translate(matrix, [0, myTime * -10, 0]);
                        AnimationSGNode.rotate(matrix, [0, 0, myTime * -120]);
                    }
                }
                AnimationSGNode.translate(matrix, [
                    0,
                    Math.sin(timeInS * Math.PI / 2) / 4,
                    Math.sin(timeInS * Math.PI / 2) / 4]);
                AnimationSGNode.rotate(matrix, [
                    Math.sin(timeInS * Math.PI / 2) * 15,
                    0,
                    0]);
            }, new RenderSGNode(
                () => {
                }, createSpaceship(res, res.imperial_triangle, {
                        head:
                            new RenderSGNode(() => {
                                defenderHeadPosition = [
                                    context.sceneMatrix[12],
                                    context.sceneMatrix[13],
                                    context.sceneMatrix[14]
                                ];
                            })
                    }
                )));
    //endregion
    //region Diamond Node
    const diamondNode = createMaterial({
        reflectiveness: 0.90,
        emission: [0.72, 0.95, 1, 1]
    }, new AnimationSGNode(undefined,
        (matrix) => {
            let myTime = timeInS / 10;
            if (activeScene === -1) {
                myTime = 0.0;
            } else if (activeScene === 0) {
                myTime %= 1.0;
                AnimationSGNode.translate(matrix, defenderHeadPosition);
                AnimationSGNode.translate(matrix, [-0.5, -0.2, 0]);
            } else if (activeScene === 1) {
                myTime -= 1;
                myTime %= 1.5;
                if (myTime <= 1) {
                    AnimationSGNode.translate(matrix, defenderHeadPosition);
                    AnimationSGNode.translate(matrix, [-0.5, -0.2, 0]);
                } else {
                    myTime -= 1;
                    let point = AnimationSGNode.bezier2(
                        myTime * 2,
                        [3 - 0.5, 0.8 - 0.2, -3],
                        [2 + 0.4, 0.85, -2.7]
                    );
                    AnimationSGNode.translate(matrix, point);
                }
            } else if (activeScene === 2) {
                myTime -= 2.5;
                myTime %= 0.5;
                // Diamond is already +50 because of Translation (Scene 3), additionally +50 in AttackerHeadPosition.
                // Therefore, "normalize" by subtracting the scene offset first.
                AnimationSGNode.translate(matrix, [0, 0, -sceneOffsets[activeScene]]);
                AnimationSGNode.translate(matrix, attackerHeadPosition);
                AnimationSGNode.translate(matrix, [0.4, 0, 0]);
            }
            AnimationSGNode.rotate(
                matrix, [-timeInS * 10, -timeInS * 10, -timeInS * 10]
            );
        }, new TransformationSGNode(
            glm.transform({
                scale: [0.1, 0.1, 0.1]
            }), [
                new HexagonRenderNode(),
                new TransformationSGNode(glm.transform({
                        translate: [0, 2, 0]
                    }),
                    new HexagonalPyramidRenderNode()),
                new TransformationSGNode(glm.transform({
                        translate: [0, -2, 0],
                        rotateX: 180
                    }),
                    new HexagonalPyramidRenderNode())]
        )));
    //endregion
    // Scene independent nodes
    //region Planet
    const planetNode =
        createMaterial({},
            new AlphaSGNode(res.asteroid_alpha,
                new AlphaBlendingSGNode(res.asteroid_texture_red,
                    new NormalSGNode(res.asteroid_normal,
                        new DisplacementSGNode(res.asteroid_displacement, 3,
                            new TextureSGNode(res.asteroid_texture,
                                createMaterial({},
                                    new AnimationSGNode(
                                        glm.transform({
                                            translate: [5, 0, -22],
                                            rotateY: 90
                                        }),
                                        (matrix) => {
                                            AnimationSGNode.rotate(
                                                matrix, [0, -timeInS, 0]
                                            )
                                        },
                                        new RenderSGNode(makeSphere(15, 250, 250)))
                                )))))));
    //endregion
    //region Mouse Pivot
    const mousePivotNode = [
        // up/down
        new AnimationSGNode(
            glm.translate(10, 0, 0),
            (matrix) => {
                AnimationSGNode.translate(matrix, [
                    -10,
                    0,
                    0
                ]);
                AnimationSGNode.rotate(matrix, [
                    0,
                    0,
                    -mouseControl.y / Math.PI * 180
                ]);
                AnimationSGNode.translate(matrix, [
                    10,
                    0,
                    0
                ]);
                //centerPivot[0] = matrix[12];
                centerPivot[1] = matrix[13];
                //centerPivot[2] = matrix[14];
            }, createMaterial({emission: [1, 0, 0, 1]},
                new RenderSGNode(() => {
                }))),
        // left/right
        new AnimationSGNode(
            glm.translate(10, 0, 0),
            (matrix) => {
                AnimationSGNode.translate(matrix, [
                    -10,
                    0,
                    0
                ]);
                AnimationSGNode.rotate(matrix, [
                    0,
                    -mouseControl.x / Math.PI * 180,
                    0
                ]);
                AnimationSGNode.translate(matrix, [
                    10,
                    0,
                    0
                ]);
                centerPivot[0] = matrix[12];
                //centerPivot[1] = matrix[13];
                centerPivot[2] = matrix[14];
            }, createMaterial({emission: [0, 1, 0, 1]},
                new RenderSGNode(() => {
                }))),
    ];
    //endregion
    //region Skybox Node
    const skyboxNode = new EnvironmentSGNode(
        res.env_pos_x, res.env_neg_x,
        res.env_pos_y, res.env_neg_y,
        res.env_pos_z, res.env_neg_z,
        [
            createMaterial({reflectiveness: 1.0},
                new SetUniformSGNode('u_isSkymapSphere', true,
                    new RenderSGNode(makeSphere(150),
                        new SetUniformSGNode('u_isSkymapSphere', false)))),
            new RenderSGNode(makeSphere(150))
        ]);
    //endregion
    //region Light Nodes
    pointLightNode = createLight({
        ambient: [0, 0, 0, 1],
        diffuse: [0.72, 0.95, 1, 1],
        specular: [0.4, 0.5, 0.6, 1]
    }, 'u_pointLight', [10, 0, 6]);
    spotLightNode = createSpotlight({
            ambient: [0.2, 1, 0, 1],
            diffuse: [0.36, 0.45, 0.5, 1]
        },
        'u_spotLight',
        [0, 0, 0],
        [0, 0, 0],
        15);
    //endregion
    //region Fire Node
    let smoke = createSmokeParticles(1000);
    let fire = createFireParticles(3000);
    let explosion = createExplosionParticles(500);
    let particleSpawnNode = new RenderSGNode(() => {
    });
    particleNode.append(new DistanceRenderNode(
        getEyePosition,
        () => defenderHeadPosition,
        10,
        new AnimationSGNode(undefined,
            (matrix) => {
                AnimationSGNode.translate(matrix, defenderHeadPosition || [0, 0, 0]);
            }, new AdvancedTextureSGNode(res.smoke,
                [
                    new ParticleSGNode(
                        smoke.start,
                        smoke.end,
                        [0, 0, 0],
                        smoke.lifetime,
                        smoke.timeOffset,
                        75.0,
                        getTime,
                        () => getTime() > 19.0,
                        [0.1, 0.1, 0.1, 0.0],
                        [0.1, 0.1, 0.1, 0.0],
                        [0.2, 0.2, 0.2, 1]),
                    new ParticleSGNode(
                        fire.start,
                        fire.end,
                        [0, 0, 0],
                        fire.lifetime,
                        fire.timeOffset,
                        25.0,
                        getTime,
                        () => getTime() > 19.0,
                        [1, 0.97, 0.7, 1],
                        [0.6, 0.08, 0.0, 1],
                        [0.2, 0.2, 0.2, 1]),
                    particleSpawnNode]))
    ));
    // generate random explosions
    for (let i = 0; i < 15; i++) {
        let randTime = Math.random() * 10 + 12;
        particleSpawnNode.append(new ParticleSGNode(
            explosion.start,
            explosion.end,
            [Math.random() * 0.5 - 1.5, Math.random() - 1, Math.random() * 0.5 - 0.5],
            explosion.lifetime,
            explosion.timeOffset,
            25.0,
            getTime,
            () => getTime() > randTime,
            [1, 0.97, 0.7, 1],
            [0.6, 0.08, 0.0, 1],
            [0.2, 0.2, 0.2, 1],
            false));
    }
    //endregion
    //endregion

    //region Nodes Graph Creation
    // All scene nodes consist of an attacker, defender and the diamond
    let baseSceneNode = new RenderSGNode(() => {
    });
    baseSceneNode.append(attackerNode);
    baseSceneNode.append(defenderNode);
    baseSceneNode.append(diamondNode);
    baseSceneNode.append(cannonNode);

    // Scene nodes (with offset for each scene, rendered only if scene is active)
    let scene0Node = new TransformationSGNode(glm.translate(0, 0, sceneOffsets[0]), [planetNode, new ConditionalRenderNode(() => activeScene === 0, baseSceneNode)]);
    let scene1Node = new TransformationSGNode(glm.translate(0, 0, sceneOffsets[1]), [planetNode, new ConditionalRenderNode(() => activeScene === 1, baseSceneNode)]);
    let scene2Node = new TransformationSGNode(glm.translate(0, 0, sceneOffsets[2]), [planetNode, new ConditionalRenderNode(() => activeScene === 2, baseSceneNode)]);

    // Order is important: render center nodes last as animation reference
    skyboxNode.append(scene0Node);
    skyboxNode.append(scene2Node);
    skyboxNode.append(scene1Node);

    // Add camera nodes
    skyboxNode.append(cameraEyeNode);
    skyboxNode.append(cameraCenterNode);
    // Add light nodes
    skyboxNode.append(pointLightNode);
    skyboxNode.append(spotLightNode);
    // Add coordinate system (if debug)
    if (debug) {
        skyboxNode.append(createCoordinateSystem([40, 40, 0]));
    }

    // Node to define which scene is entered/exited
    rootNode.append(new ConditionalRenderNode(() => !followCamera, [
        new DistanceTriggerNode(() => [0, 0, sceneOffsets[0]], getEyePosition, 25, (entered) => {
            if (entered) {
                console.log("Entered Scene 1");
                activeScene = 0;
            } else {
                console.log("Left Scene 1");
                activeScene = -1;
            }
        }),
        new DistanceTriggerNode(() => [0, 0, sceneOffsets[1]], getEyePosition, 25, (entered) => {
            if (entered) {
                console.log("Entered Scene 2");
                activeScene = 1;
            } else {
                console.log("Left Scene 2");
                activeScene = -1;
            }
        }),
        new DistanceTriggerNode(() => [0, 0, sceneOffsets[2]], getEyePosition, 25, (entered) => {
            if (entered) {
                console.log("Entered Scene 3");
                activeScene = 2;
            } else {
                console.log("Left Scene 3");
                activeScene = -1;
            }
        })
    ]));
    // Enable light only when a scene is active
    rootNode.append(new ConditionalRenderNode(() => activeScene >= 0, [
        new SetUniformSGNode('u_enableLight', 1),
        new SetUniformSGNode('u_enableSpotlight', 1)]));
    // Add mouse pivots
    rootNode.append(mousePivotNode[0]);
    rootNode.append(mousePivotNode[1]);
    // Add skybox
    rootNode.append(skyboxNode);
    //endregion
}

// region Particle System Functions
/**
 * Creates a new (and empty) particle system.
 * @returns {{lifetime: Array, timeOffset: Array, start: Array, end: Array}}
 */
function createParticleSystem() {
    // particle system type
    return {
        // the lifetime of each particle
        lifetime: [],
        // a random offset for each particle (so that they don't always start at 0)
        timeOffset: [],
        // the particle start position
        start: [],
        // the particle end position
        end: []
    };
}

/**
 * Creates a new particle system filled with particles behaving like a fire.
 * @param num Number of particles to be generated.
 * @returns {{lifetime: Array, timeOffset: Array, start: Array, end: Array}}
 */
function createFireParticles(num) {
    let ps = createParticleSystem();
    for (let i = 0; i < num; i++) {
        // lifetime is between 4s and 8s
        ps.lifetime.push(Math.random() * 4 + 4);
        // offset is between 0s and 4s
        ps.timeOffset.push(Math.random() * 4);
        // start (x,y,z) is between -0.125 and 0.125 (world coordinates)
        ps.start.push((Math.random() * 0.25) - 0.125);
        ps.start.push((Math.random() * 0.25) - 0.125);
        ps.start.push((Math.random() * 0.25) - 0.125);
        // end (x,z) is between -0.5 and 0.5 (world coordinates)
        // end (y) is elevated by 1 to simulate moving upwards.
        ps.end.push((Math.random() - 0.5));
        ps.end.push((Math.random() + 1));
        ps.end.push((Math.random() - 0.5));
    }
    return ps;
}

/**
 * Creates a new particle system filled with particles behaving like smoke.
 * @param num Number of particles to be generated.
 * @returns {{lifetime: Array, timeOffset: Array, start: Array, end: Array}}
 */
function createSmokeParticles(num) {
    let ps = createParticleSystem();
    for (let i = 0; i < num; i++) {
        // lifetime is between 5s and 11s
        ps.lifetime.push(Math.random() * 6 + 5);
        // offset is between 0s and 6s
        ps.timeOffset.push(Math.random() * 6);
        // start (x,y,z) is between -0.125 and 0.125 (world coordinates)
        ps.start.push((Math.random() * 0.25) - 0.125);
        ps.start.push((Math.random() * 0.25) - 0.125);
        ps.start.push((Math.random() * 0.25) - 0.125);
        // end (x,z) is between -0.5 and 0.5 (world coordinates)
        // end (y) is elevated by 2 to simulate moving upwards.
        ps.end.push((Math.random() - 0.5));
        ps.end.push((Math.random() + 2));
        ps.end.push((Math.random() - 0.5));
    }
    return ps;
}

/**
 * Creates a new particle system filled with particles behaving like an explosion.
 * @param num Number of particles to be generated.
 * @returns {{lifetime: Array, timeOffset: Array, start: Array, end: Array}}
 */
function createExplosionParticles(num) {
    let ps = createParticleSystem();
    for (let i = 0; i < num; i++) {
        // lifetime is between 1.2s and 2.2s
        ps.lifetime.push(Math.random() * 1.2 + 1);
        // offset is between 0s and 0.2s
        ps.timeOffset.push(Math.random() * 0.2);
        // start (x,y,z) is between 0 and 0.25 (world coordinates)
        ps.start.push((Math.random() * 0.25));
        ps.start.push((Math.random() * 0.25));
        ps.start.push((Math.random() * 0.25));
        // end (x,y,z) is between -1 and 1 (world coordinates)
        // to simulate an explosion
        ps.end.push((Math.random() * 2 - 1));
        ps.end.push((Math.random() * 2 - 1));
        ps.end.push((Math.random() * 2 - 1));
    }
    return ps;
}
//endregion

/**
 * Gets current time.
 * @returns {number} Current time in s.
 */
function getTime() {
    return timeInS;
}

/**
 * Gets current eye position.
 * @returns {vec3} Eye position dependent on viewing mode.
 */
function getEyePosition() {
    if (followCamera) {
        return eyeAnimation;
    } else {
        return eye;
    }
}

/**
 * Generates a new self-destructing bullet-node as child of the given node.
 * @param node Node where the bullet should be appended.
 */
function shoot(node) {
    // save current time (for animation purposes)
    let time = getTime();
    // append a new bullet and calculate shoot direction (i.e. randomize direction a little -> more realistic)
    let projectileNode = node.append(
        new AnimationSGNode(glm.transform({
                rotateY: Math.floor((Math.random() * 14) - 7),
                rotateZ: Math.floor((Math.random() * 14) - 7)
            }),
            function (matrix) {
                // animate along x-Axis
                AnimationSGNode.translate(matrix, [(getTime() - time) * 5, 0, 0]);
            },
            new TransformationSGNode(glm.transform({
                scale: [0.03, 0.03, 0.03],
                rotateZ: 90
            }), new HexagonRenderNode())));
    // remove node after 1-2s
    setTimeout(() => {
        node.remove(projectileNode);
    }, Math.random() * 1000 + 1000);
}

/**
 * Render the scene dependent on the current time.
 * @param timeInMs Time in ms.
 */
function render(timeInMs) {
    // calculate correct time in S (with offset)
    timeInS = (timeInMs / 1000) % (30.1 - timeOffset) + timeOffset;
    if (!followCamera) {
        // if we are not following the camera, define the time by the active scene
        if (activeScene === 0) {
            // 1. Scene: 0s to 10s
            timeInS %= 10;
        } else if (activeScene === 1) {
            // 2. Scene: 10s to 25s
            timeInS %= 15;
            timeInS += 10;
        } else if (activeScene === 2) {
            // 3. Scene: 25s to 30s
            timeInS %= 5;
            timeInS += 25;
        }
    } else {
        // if we are following the camera, define the active scenes by time offsets
        if (timeInS < 10) {
            activeScene = 0;
        } else if (timeInS < 25) {
            activeScene = 1;
        } else if (timeInS < 30) {
            activeScene = 2;
        }
    }
    // write time in scene
    document.getElementById("time").innerHTML = (Math.round(timeInS * 100) / 100).toFixed(2).toString().padStart(5, "0");
    // display current effects
    displayText("Planet: Displacement Map + Alpha Blending with alpha and two textures\n" +
        "Ships: Animations + Materials + Textures (Pyramid) + Alpha Map (Head)\n" +
        "Spotlight (moving) attached to attacker's head\n" +
        "Point light lighting the overall scene\n" +
        "Particle system (fire and smoke)");


    // display intro text (if time < 7s --> CSS Animation will make it invisible)
    if (timeInS < 7 && followCamera) {
        createCustomHtmlText(gl.canvas, "introText");
        displayCustomText("introText", "<span class=\"text-big\">STAR WARS</span><br /><br />" +
            "<span>A long time ago in a galaxy far, far away....<br />" +
            "The Galactic Empire has stolen an important artifact<br />" +
            "and the Rebels do their utmost to return it.</span>");
    } else {
        deleteCustomHtmlText("introText");
    }

    // display outro text (if time > 28 --> CSS Animation will make it invisible)
    if (timeInS > 28 && followCamera) {
        createCustomHtmlText(gl.canvas, "endText");
        displayCustomText("endText", "<span class=\"text-big\">THE END</span>");
    } else {
        deleteCustomHtmlText("endText");
    }

    // shoot bullets if time is between 10 and 20 and the last shot was before 0.1-0.15ms
    if (timeInS > 10.0 && timeInS < 20) {
        if (timeInS - lastShot > Math.random() * 0.05 + 0.10) {
            lastShot = timeInS;
            shoot(cannonRightNode);
            shoot(cannonLeftNode);
        }
    } else {
        lastShot = 0.0;
    }

    // set gray background (invisible because of skymap)
    gl.clearColor(0.15, 0.15, 0.15, 1.0);
    //clear the buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // create the scene context
    context = createSGContext(gl,
        mat4.perspective(mat4.create(), glm.deg2rad(60), aspectRatio, 0.001, 1000));

    //region Controls and view matrix
    if (followCamera) {
        // set camera to eye and center animation pivots
        context.viewMatrix = mat4.lookAt(mat4.create(),
            eyeAnimation,
            centerAnimation,
            upAnimation);
    } else {
        // calculate direction vectors from eye and center pivots
        let direction = vec3.normalize(vec3.create(),
            vec3.subtract(vec3.create(), centerPivot, vec3.fromValues(0, 0, 0)));
        let directionRight = vec3.normalize(vec3.create(),
            vec3.cross(vec3.create(), direction, up));
        let directionUp = vec3.normalize(vec3.create(),
            vec3.cross(vec3.create(), direction, directionRight));
        // scale direction vectors to define the speed of movement
        direction = vec3.scale(vec3.create(), direction, 0.4);
        directionRight = vec3.scale(vec3.create(), directionRight, 0.4);
        directionUp = vec3.scale(vec3.create(), directionUp, 0.4);
        // check if controls are pressed and move camera accordingly
        if (keyboardControl.w()) {
            eye = vec3.add(vec3.create(), eye, direction);
        }
        if (keyboardControl.s()) {
            eye = vec3.subtract(vec3.create(), eye, direction);
        }
        if (keyboardControl.a()) { // A
            eye = vec3.subtract(vec3.create(), eye, directionRight);
        }
        if (keyboardControl.d()) { // D
            eye = vec3.add(vec3.create(), eye, directionRight);
        }
        if (keyboardControl.f()) { // F
            eye = vec3.add(vec3.create(), eye, directionUp);
        }
        if (keyboardControl.r()) { // R
            eye = vec3.subtract(vec3.create(), eye, directionUp);
        }
        // center is always the eye facing the direction
        let center = vec3.add(vec3.create(), eye, direction);
        context.viewMatrix = mat4.lookAt(mat4.create(),
            eye,
            center,
            up);
        // save eye to the local storage
        localStorage.setItem("eye", JSON.stringify([eye[0], eye[1], eye[2]]));
        // save mouseX and mouseY movements to local storage
        localStorage.setItem("mouseX", JSON.stringify(mouseControl.x));
        localStorage.setItem("mouseY", JSON.stringify(mouseControl.y));
    }
    // get inverse view matrix to allow to compute viewing direction in world space for environment mapping
    context.invViewMatrix = mat4.invert(mat4.create(), context.viewMatrix);
    //endregion

    // update ui components
    if (activeScene >= 0) {
        spotLightNode.position = vec3.add(vec3.create(), attackerHeadPosition, vec3.fromValues(0, 0, sceneOffsets[activeScene]));
        pointLightNode.position = vec3.add(vec3.create(), vec3.fromValues(10, 0, 6), vec3.fromValues(0, 0, sceneOffsets[activeScene]));
        spotLightNode.direction = vec3.subtract(vec3.create(), defenderHeadPosition, attackerHeadPosition);
    }

    // for rendering the standard scene:
    // enable depth mask and depth test to occlude objects farther away
    // use blending with function SRC_RGB * {SRC_ALPHA} + DST_RGB * {ONE_MINUS_SRC_ALPHA}
    //activate the standard shader program
    gl.useProgram(program);
    gl.depthMask(true);
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    // render standard scene
    rootNode.render(context);

    // for rendering the particles:
    // disable depth mask and depth test to create a smooth particle look
    // use blending with function SRC_RGB * {SRC_ALPHA} + DST_RGB * {ONE}
    //activate the particle shader program
    gl.useProgram(particleProgram);
    gl.depthMask(false);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
    // render particles
    particleNode.render(context);

    //request another render call as soon as possible
    requestAnimationFrame(render);
}

/**
 * Loads default values from local storage (if possible)
 */
function loadDefaults() {
    let item;
    if (item = localStorage.getItem("eye")) {
        eye = JSON.parse(item);
    }
    if (item = localStorage.getItem("up")) {
        up = JSON.parse(item);
    }
    if (item = localStorage.getItem("mouseX")) {
        mouseControl.x = JSON.parse(item);
    }
    if (item = localStorage.getItem("mouseY")) {
        mouseControl.y = JSON.parse(item);
    }
    if (item = localStorage.getItem("followCamera")) {
        followCamera = JSON.parse(item);
    }
    if (item = localStorage.getItem("debug")) {
        debug = JSON.parse(item);
    }
    if (item = localStorage.getItem("timeOffset")) {
        timeOffset = JSON.parse(item);
        document.getElementById("offsetSlider").value = timeOffset;
        changeTimeOffset(timeOffset);
    }
}

/**
 * Inverts the current viewing mode and saves it in the local storage.
 */
function changeViewMode() {
    followCamera = !followCamera;

    localStorage.setItem("followCamera", JSON.stringify(followCamera));
}

/**
 * Changes the time offset and saves it in the local storage.
 * @param offset Offset to be applied.
 */
function changeTimeOffset(offset) {
    timeOffset = parseInt(offset);
    document.getElementById("offset").innerHTML = (Math.round(timeOffset * 100) / 100).toFixed(2).toString().padStart(5, "0");

    localStorage.setItem("timeOffset", JSON.stringify(timeOffset));
}

/**
 * Inverts the current debug mode, saves it in the local storage and reloads the page.
 */
function changeDebug() {
    debug = !debug;

    localStorage.setItem("debug", JSON.stringify(debug));
    // force page to reload
    location.reload();
}

/**
 * Creates a custom <p> tag with given ID below the canvas element.
 * @param canvas Canvas element as reference point.
 * @param id ID of the new tag.
 */
function createCustomHtmlText(canvas, id) {
    if (!document.getElementById(id)) {
        let par = document.createElement('p');
        par.id = id;
        par.className = 'custom-paragraph';
        par.style.width = canvas.clientWidth + 'px';
        par.style.top = (canvas.clientHeight / 2 + 50) + 'px';
        let text = document.createTextNode('');
        par.appendChild(text);
        document.body.appendChild(par);
    }
}

/**
 * Deletes a custom <p> tag with given ID.
 * @param id ID of the element to be deleted.
 */
function deleteCustomHtmlText(id) {
    if (document.getElementById(id))
        document.getElementById(id).outerHTML = "";
}

/**
 * Displays a given text in an element with given ID.
 * @param id ID of the element.
 * @param text Text to be displayed.
 */
function displayCustomText(id, text) {
    if (document.getElementById(id))
        document.getElementById(id).innerHTML = text;
}

// load all resources, init and finally render the scene.
loadResources({
    master_vs: 'shader/master.vs.glsl',
    master_fs: 'shader/master.fs.glsl',
    particle_vs: 'shader/particle.vs.glsl',
    particle_fs: 'shader/particle.fs.glsl',
    asteroid_texture: 'textures/asteroid/asteroid_tex.png',
    asteroid_texture_red: 'textures/asteroid/asteroid_tex_red.png',
    asteroid_alpha: 'textures/asteroid/asteroid_alpha.png',
    asteroid_displacement: 'textures/asteroid/asteroid_dis.png',
    asteroid_normal: 'textures/asteroid/asteroid_normal.png',
    cracks: 'textures/cracks.png',
    rebels_triangle: 'textures/rebels_triangle.png',
    imperial_triangle: 'textures/imperial_triangle.png',
    env_pos_x: 'textures/skybox/skybox_RT.jpg',
    env_neg_x: 'textures/skybox/skybox_LF.jpg',
    env_pos_y: 'textures/skybox/skybox_DN.jpg',
    env_neg_y: 'textures/skybox/skybox_UP.jpg',
    env_pos_z: 'textures/skybox/skybox_FT.jpg',
    env_neg_z: 'textures/skybox/skybox_BK.jpg',
    smoke: 'textures/smoke.gif',
}).then(function (res) {
    init(res);
    render();
});