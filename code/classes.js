//region Custom Object Nodes
/**
 * Custom RenderSGNode rendering a cube (without texture-coordinates -> not needed).
 */
class CubeRenderNode extends RenderSGNode {
    constructor() {
        super({
            position: new Float32Array([
                // top
                -1, -1, -1,
                +1, -1, -1,
                +1, +1, -1,
                -1, +1, -1,
                // bottom
                -1, -1, +1,
                +1, -1, +1,
                +1, +1, +1,
                -1, +1, +1,
                // right
                -1, -1, -1,
                -1, +1, -1,
                -1, +1, +1,
                -1, -1, +1,
                // left
                +1, -1, -1,
                +1, +1, -1,
                +1, +1, +1,
                +1, -1, +1,
                // back
                -1, -1, -1,
                -1, -1, +1,
                +1, -1, +1,
                +1, -1, -1,
                // front
                -1, +1, -1,
                -1, +1, +1,
                +1, +1, +1,
                +1, +1, -1
            ]),
            normal: new Float32Array([
                // top
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // bottom
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // right
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                // left
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,
                // back
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,
                // front
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
            ]),
            texture: null,
            index: new Float32Array([
                0, 1, 2, 0, 2, 3,
                4, 5, 6, 4, 6, 7,
                8, 9, 10, 8, 10, 11,
                12, 13, 14, 12, 14, 15,
                16, 17, 18, 16, 18, 19,
                20, 21, 22, 20, 22, 23
            ])
        });
    }
}

/**
 * Custom RenderSGNode rendering a fully defined pyramid.
 */
class PyramidRenderNode extends RenderSGNode {
    constructor() {
        super({
            position: new Float32Array([
                // left top 0-2
                +0, +0, +0,
                -1, -1, +1,
                -1, -1, -1,
                // up top 3-5
                +0, +0, +0,
                -1, -1, -1,
                +1, -1, -1,
                // right top 6-8
                +0, +0, +0,
                +1, -1, -1,
                +1, -1, +1,
                // down top 9-11
                +0, +0, +0,
                +1, -1, +1,
                -1, -1, +1,
                // left top back 12-14
                -1, -1, -1,
                +1, -1, -1,
                -1, -1, +1,
                // right bottom back 15-17
                +1, -1, -1,
                +1, -1, +1,
                -1, -1, +1,
            ]),
            normal: new Float32Array([
                -1, +1, +0,
                -1, +1, +0,
                -1, +1, +0,
                +0, +1, -1,
                +0, +1, -1,
                +0, +1, -1,
                +1, +1, +0,
                +1, +1, +0,
                +1, +1, +0,
                +0, +1, +1,
                +0, +1, +1,
                +0, +1, +1,
                +0, -1, +0,
                +0, -1, +0,
                +0, -1, +0,
                +0, -1, +0,
                +0, -1, +0,
                +0, -1, +0,
            ]),
            texture: new Float32Array([
                // left top 0-2
                0.5, 0.5,
                0.0, 1.0,
                0.0, 0.0,
                // up top 3-5
                0.5, 0.5,
                0.0, 0.0,
                1.0, 0.0,
                // right top 6-8
                0.5, 0.5,
                1.0, 0.0,
                1.0, 1.0,
                // down top 9-11
                0.5, 0.5,
                1.0, 1.0,
                0.0, 1.0,
                // left top back 12-14
                0.0, 0.0,
                1.0, 0.0,
                0.0, 1.0,
                // right bottom back 15-17
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,
                // additional vertices (required for whatever reason..)
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
            ]),
            index: new Float32Array([
                0, 1, 2,
                3, 4, 5,
                6, 7, 8,
                9, 10, 11,
                12, 13, 14,
                15, 16, 17
            ])
        });
    }
}

/**
 * Custom RenderSGNode rendering a hexagonal pyramid (without texture-coordinates -> not needed).
 */
class HexagonalPyramidRenderNode extends RenderSGNode {
    constructor() {
        super({
            position: new Float32Array([
                // top
                0, 0, 0,
                1 / 3, -1, -1,
                -1 / 3, -1, -1,
                // top right
                0, 0, 0,
                1, -1, -1 / 3,
                1 / 3, -1, -1,
                // right
                0, 0, 0,
                1, -1, 1 / 3,
                1, -1, -1 / 3,
                // bottom right
                0, 0, 0,
                1, -1, 1 / 3,
                1 / 3, -1, 1,
                // bottom
                0, 0, 0,
                1 / 3, -1, 1,
                -1 / 3, -1, 1,
                // bottom left
                0, 0, 0,
                -1, -1, 1 / 3,
                -1 / 3, -1, 1,
                // left
                0, 0, 0,
                -1, -1, 1 / 3,
                -1, -1, -1 / 3,
                // top left
                0, 0, 0,
                -1, -1, -1 / 3,
                -1 / 3, -1, -1,
            ]),
            normal: new Float32Array([
                // top
                0, 1, -1,
                0, 1, -1,
                0, 1, -1,
                // top right
                1, 1, -1,
                1, 1, -1,
                1, 1, -1,
                // right
                1, 1, 0,
                1, 1, 0,
                1, 1, 0,
                // bottom right
                1, 1, 1,
                1, 1, 1,
                1, 1, 1,
                // bottom
                0, 1, 1,
                0, 1, 1,
                0, 1, 1,
                // bottom left
                -1, 1, 1,
                -1, 1, 1,
                -1, 1, 1,
                // left
                -1, 1, 0,
                -1, 1, 0,
                -1, 1, 0,
                // top left
                -1, 1, -1,
                -1, 1, -1,
                -1, 1, -1,
            ]),
            texture: null,
            index: new Float32Array([
                0, 1, 2,
                3, 4, 5,
                6, 7, 8,
                9, 10, 11,
                12, 13, 14,
                15, 16, 17,
                18, 19, 20,
                21, 22, 23
            ])
        });
    }
}

/**
 * Custom RenderSGNode rendering a hexagon (without texture-coordinates -> not needed).
 */
class HexagonRenderNode extends RenderSGNode {
    constructor() {
        super({
            position: new Float32Array([
                // back face
                // top
                0, -1, 0,
                1 / 3, -1, -1,
                -1 / 3, -1, -1,
                // top right
                0, -1, 0,
                1, -1, -1 / 3,
                1 / 3, -1, -1,
                // right
                0, -1, 0,
                1, -1, 1 / 3,
                1, -1, -1 / 3,
                // bottom right
                0, -1, 0,
                1, -1, 1 / 3,
                1 / 3, -1, 1,
                // bottom
                0, -1, 0,
                1 / 3, -1, 1,
                -1 / 3, -1, 1,
                // bottom left
                0, -1, 0,
                -1, -1, 1 / 3,
                -1 / 3, -1, 1,
                // left
                0, -1, 0,
                -1, -1, 1 / 3,
                -1, -1, -1 / 3,
                // top left
                0, -1, 0,
                -1, -1, -1 / 3,
                -1 / 3, -1, -1,

                // front face
                // top
                0, 1, 0,
                1 / 3, 1, -1,
                -1 / 3, 1, -1,
                // top right
                0, 1, 0,
                1, 1, -1 / 3,
                1 / 3, 1, -1,
                // right
                0, 1, 0,
                1, 1, 1 / 3,
                1, 1, -1 / 3,
                // bottom right
                0, 1, 0,
                1, 1, 1 / 3,
                1 / 3, 1, 1,
                // bottom
                0, 1, 0,
                1 / 3, 1, 1,
                -1 / 3, 1, 1,
                // bottom left
                0, 1, 0,
                -1, 1, 1 / 3,
                -1 / 3, 1, 1,
                // left
                0, 1, 0,
                -1, 1, 1 / 3,
                -1, 1, -1 / 3,
                // top left
                0, 1, 0,
                -1, 1, -1 / 3,
                -1 / 3, 1, -1,

                // round
                // top
                1 / 3, 1, -1,
                -1 / 3, 1, -1,
                -1 / 3, -1, -1,

                1 / 3, 1, -1,
                1 / 3, -1, -1,
                -1 / 3, -1, -1,

                // top right
                1, 1, -1 / 3,
                1 / 3, 1, -1,
                1 / 3, -1, -1,

                1, 1, -1 / 3,
                1, -1, -1 / 3,
                1 / 3, -1, -1,

                // right
                1, 1, 1 / 3,
                1, -1, 1 / 3,
                1, -1, -1 / 3,

                1, -1, -1 / 3,
                1, 1, 1 / 3,
                1, 1, -1 / 3,

                // bottom right
                1, 1, 1 / 3,
                1, -1, 1 / 3,
                1 / 3, -1, 1,

                1 / 3, -1, 1,
                1, 1, 1 / 3,
                1 / 3, 1, 1,

                // bottom
                1 / 3, 1, 1,
                -1 / 3, 1, 1,
                -1 / 3, -1, 1,

                1 / 3, 1, 1,
                1 / 3, -1, 1,
                -1 / 3, -1, 1,

                // bottom left
                -1, 1, 1 / 3,
                -1, -1, 1 / 3,
                -1 / 3, -1, 1,

                -1 / 3, -1, 1,
                -1, 1, 1 / 3,
                -1 / 3, 1, 1,

                // left
                -1, 1, 1 / 3,
                -1, -1, 1 / 3,
                -1, -1, -1 / 3,

                -1, -1, -1 / 3,
                -1, 1, 1 / 3,
                -1, 1, -1 / 3,

                // top left
                -1, 1, -1 / 3,
                -1 / 3, 1, -1,
                -1 / 3, -1, -1,

                -1, 1, -1 / 3,
                -1, -1, -1 / 3,
                -1 / 3, -1, -1,

            ]),
            normal: new Float32Array([
                // back face
                // top
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // top right
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // right
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // bottom right
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // bottom
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // bottom left
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // left
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,
                // top left
                0, -1, 0,
                0, -1, 0,
                0, -1, 0,

                // front face
                // top
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // top right
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // right
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // bottom right
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // bottom
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // bottom left
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // left
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                // top left
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,

                // round
                // top
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,

                // top right
                1, 0, -1,
                1, 0, -1,
                1, 0, -1,
                1, 0, -1,
                1, 0, -1,
                1, 0, -1,

                // right
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,
                1, 0, 0,

                // bottom right
                1, 0, 1,
                1, 0, 1,
                1, 0, 1,
                1, 0, 1,
                1, 0, 1,
                1, 0, 1,

                // bottom
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,
                0, 0, -1,

                // bottom left
                -1, 0, 1,
                -1, 0, 1,
                -1, 0, 1,
                -1, 0, 1,
                -1, 0, 1,
                -1, 0, 1,

                // left
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,
                -1, 0, 0,

                // top left
                -1, 0, -1,
                -1, 0, -1,
                -1, 0, -1,
                -1, 0, -1,
                -1, 0, -1,
                -1, 0, -1,

            ]),
            texture: null,
            index: new Float32Array([
                // back face
                0, 1, 2,
                3, 4, 5,
                6, 7, 8,
                9, 10, 11,
                12, 13, 14,
                15, 16, 17,
                18, 19, 20,
                21, 22, 23,
                // front face
                24, 25, 26,
                27, 28, 29,
                30, 31, 32,
                33, 34, 35,
                36, 37, 38,
                39, 40, 41,
                42, 43, 44,
                45, 46, 47,
                // round
                48, 49, 50,
                51, 52, 53,
                54, 55, 56,
                57, 58, 59,
                60, 61, 62,
                63, 64, 65,
                66, 67, 68,
                69, 70, 71,
                72, 73, 74,
                75, 76, 77,
                78, 79, 80,
                81, 82, 83,
                84, 85, 86,
                87, 88, 89,
                90, 91, 92,
                93, 94, 95
            ])
        });
    }
}
//endregion

//region Custom Nodes
/**
 * Custom LightSGNode for setting the uniform 'u_enableLight' while rendering.
 */
class AdvancedLightSGNode extends LightSGNode {
    constructor(position, children) {
        super(position, children);
        this.enableUniform = 'u_enableLight';
    }

    render(context) {
        gl.uniform1i(gl.getUniformLocation(context.shader, this.enableUniform), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, this.enableUniform), 0);
    }
}

/**
 * Custom LightSGNode for SpotLights.
 */
class SpotlightSGNode extends AdvancedLightSGNode {
    /**
     * Creates a new SpotlightSGNode with given parameters.
     * Sets the uniform 'u_enableSpotlight' while rendering.
     * @param position Position of the light.
     * @param direction Direction vector of the light.
     * @param limit Limit of light in degrees. Light is emitted in direction +- limit.
     * @param children Child nodes to be rendered.
     */
    constructor(position, direction, limit, children) {
        super(position, children);
        this.direction = direction;
        this.limit = Math.cos(glm.deg2rad(limit));
        this.enableUniform = 'u_enableSpotlight';
    }

    setLightUniforms(context) {
        super.setLightUniforms(context);
        const gl = context.gl;
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform + 'Limit'), this.limit);
        gl.uniform3f(gl.getUniformLocation(context.shader, this.uniform + 'Direction'), this.direction[0], this.direction[1], this.direction[2]);
    }
}

/**
 * Custom AdvancedTextureSGNode for setting the uniform 'u_enableTexture' while rendering.
 */
class TextureSGNode extends AdvancedTextureSGNode {
    render(context) {
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableTexture'), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableTexture'), 0);
    }
}

/**
 * Custom AdvancedTextureSGNode for displacement shading.
 * @see AdvancedTextureSGNode
 */
class DisplacementSGNode extends AdvancedTextureSGNode {
    /**
     * Creates a new DisplacementSGNode with given parameters.
     * Also sets textureunit to 1 and the uniform to 'u_dis' (in AdvancedTextureSGNode).
     * While rendering, the uniform 'u_enableDisplacement' is set to 1.
     * @param image Image texture to be used as displacement map.
     * @param displacementFactor Factor of displacement.
     * @param children Child nodes to be rendered with displacement mapping.
     */
    constructor(image, displacementFactor, children) {
        super(image, children);
        this.displacementFactor = displacementFactor;
        this.textureunit = 1;
        this.uniform = 'u_dis';
    }

    render(context) {
        gl.uniform1f(gl.getUniformLocation(context.shader, 'u_displacementFactor'), this.displacementFactor);

        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableDisplacement'), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableDisplacement'), 0);
    }
}

/**
 * Custom AdvancedTextureSGNode for normal shading from a texture.
 * @see AdvancedTextureSGNode
 */
class NormalSGNode extends AdvancedTextureSGNode {
    /**
     * Creates a new NormalSGNode with given parameters.
     * Also sets textureunit to 5 and the uniform to 'u_normal' (in AdvancedTextureSGNode).
     * While rendering, the uniform 'u_enableNormal' is set to 1.
     * @param image Image texture to be used as normal map.
     * @param children Child nodes to be rendered with normal mapping.
     */
    constructor(image, children) {
        super(image, children);
        this.textureunit = 5;
        this.uniform = 'u_normal';
    }

    render(context) {
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableNormal'), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableNormal'), 0);
    }
}

/**
 * Custom AdvancedTextureSGNode for rendering an alpha texture.
 */
class AlphaSGNode extends AdvancedTextureSGNode {
    /**
     * Creates a new AlphaSGNode with given parameters.
     * Sets the uniform 'u_enableAlpha' while rendering.
     * @param image Texture of an alpha map.
     * @param children Children to be rendered with the alpha map.
     */
    constructor(image, children) {
        super(image, children);
        this.textureunit = 2;
        this.uniform = 'u_alpha';
    }

    render(context) {
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableAlpha'), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableAlpha'), 0);
    }
}

/**
 * Custom AdvancedTextureSGNode for rendering another texture behind an alpha texture.
 * Use in combination with an {AlphaSGNode}.
 * @see AlphaSGNode
 */
class AlphaBlendingSGNode extends AdvancedTextureSGNode {
    /**
     * Creates a new AlphaBlendingSGNode with given parameters.
     * Sets the uniform 'u_enableAlphaBlending' while rendering.
     * @param image Texture to be blended with the underlying alpha map.
     * @param children Children to be rendered.
     */
    constructor(image, children) {
        super(image, children);
        this.textureunit = 3;
        this.uniform = 'u_alphaBlending';
    }

    render(context) {
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableAlphaBlending'), 1);
        super.render(context);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableAlphaBlending'), 0);
    }
}

/**
 * Custom SGNode rendering it's children only if a condition returns true.
 */
class ConditionalRenderNode extends SGNode {
    /**
     * Creates a new ConditionalRenderNode with given parameters.
     * @param conditionFunc Function to determine if the children should be rendered (or not).
     * @param children Children to be rendered.
     */
    constructor(conditionFunc, children) {
        super(children);
        this.conditionFunc = conditionFunc;
    }

    render(context) {
        if (this.conditionFunc()) {
            super.render(context);
        }
    }
}

/**
 * Custom SGNode triggering if a point is within a defined distance to another point.
 */
class DistanceTriggerNode extends SGNode {
    /**
     * Creates a new DistanceTriggerNode with given parameters.
     * @param originFunc Function providing the origin point.
     * @param referenceFunc Function providing the reference point.
     * @param distance Comparison distance between these two points.
     * @param triggerFunc Function to be triggered when actual distance is smaller than comparison distance.
     * Provides boolean parameter indicating if area was entered or exited.
     * @param once Defines if event is triggered once per enter/exit or always when within the distance.
     */
    constructor(originFunc, referenceFunc, distance, triggerFunc, once = true) {
        super();
        this.origin = originFunc;
        this.reference = referenceFunc;
        this.distance = distance;
        this.triggerFunc = triggerFunc;
        this.once = once;
        this.entered = false;
    }

    render(context) {
        if (vec3.distance(this.origin(), this.reference()) <= this.distance) {
            if (this.once && this.entered) {
                return;
            }
            this.entered = true;
            this.triggerFunc(this.entered);
        } else {
            if (this.once && !this.entered) {
                return;
            }
            this.entered = false;
            this.triggerFunc(this.entered);
        }
    }
}

/**
 * Custom DistanceTriggerNode rendering it's children when within range.
 * @see DistanceTriggerNode
 */
class DistanceRenderNode extends DistanceTriggerNode {
    /**
     * Creates a new DistanceRenderNode with given parameters.
     * @param originFunc Function providing the origin point.
     * @param referenceFunc Function providing the reference point.
     * @param distance Comparison distance between these two points.
     * @param children Child nodes to be rendered when within distance.
     */
    constructor(originFunc, referenceFunc, distance, children) {
        super(originFunc, referenceFunc, distance,
            () => {
            }, true);
        this.children = typeof children !== 'undefined' ? [].concat(children) : [];
    }

    render(context) {
        // render DistanceTriggerNode to update status
        super.render(context);

        if (this.entered) {
            this.children.forEach(c => c.render(context));
        }
    }
}

/**
 * Custom RenderSGNode rendering a particle system.
 */
class ParticleSGNode extends SGNode {
    /**
     * Creates a new ParticleSGNode with given parameters.
     * @param start Start position (as single array).
     * @param end End position (as single array).
     * @param offset Offset position.
     * @param lifetime Lifetime of the particles (as single array).
     * @param timeOffset Time offset of when the particles should start being emitted (as single array).
     * @param pointSize Point size of the particles.
     * @param timeFunc Function returning the current time.
     * @param startedFunc Function returning true if emission should start, false otherwise.
     * @param startColor Start color of the color ramp.
     * @param middleColor Middle color of the color ramp.
     * @param endColor End color of the color ramp.
     * @param repeat True if dead particles should become alive again. False otherwise.
     */
    constructor(start, end, offset, lifetime, timeOffset, pointSize, timeFunc, startedFunc, startColor, middleColor, endColor, repeat = true) {
        super();
        this.start = start;
        this.end = end;
        this.lifetime = lifetime;
        this.timeOffset = timeOffset;
        this.pointSize = pointSize;
        this.time = timeFunc;
        this.started = startedFunc;
        this.offset = offset;
        this.startColor = startColor;
        this.middleColor = middleColor;
        this.endColor = endColor;
        this.repeat = repeat;
        this.lastTime = -1.0;

        //number of particles
        this.numItems = lifetime.length;
        this.startBuffer = null;
        this.endBuffer = null;
        this.lifetimeBuffer = null;
        this.timeOffsetBuffer = null;
    }

    //first time initBuffer of buffers
    initBuffer(gl) {
        this.startBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.startBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.start), gl.STATIC_DRAW);

        this.endBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.endBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.end), gl.STATIC_DRAW);

        this.lifetimeBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.lifetimeBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.lifetime), gl.STATIC_DRAW);

        this.timeOffsetBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.timeOffsetBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.timeOffset), gl.STATIC_DRAW);
    }

    render(context) {
        super.render(context);
        let gl = context.gl;
        let shader = context.shader;
        if (!shader) {
            return;
        }
        if (this.startBuffer === null) {
            //lazy initBuffer
            this.initBuffer(gl);
        }
        //set attributes
        gl.bindBuffer(gl.ARRAY_BUFFER, this.lifetimeBuffer);
        let lifetimeLoc = gl.getAttribLocation(shader, 'a_lifetime');
        gl.vertexAttribPointer(lifetimeLoc, 1, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(lifetimeLoc);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.timeOffsetBuffer);
        let timeOffsetLoc = gl.getAttribLocation(shader, 'a_timeOffset');
        gl.vertexAttribPointer(timeOffsetLoc, 1, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(timeOffsetLoc);

        let startLoc = gl.getAttribLocation(shader, 'a_start');
        gl.bindBuffer(gl.ARRAY_BUFFER, this.startBuffer);
        gl.vertexAttribPointer(startLoc, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(startLoc);

        let endLoc = gl.getAttribLocation(shader, 'a_end');
        gl.bindBuffer(gl.ARRAY_BUFFER, this.endBuffer);
        gl.vertexAttribPointer(endLoc, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(endLoc);

        // correct to always start from 0.0
        if (this.lastTime < 0.0 && this.started()) {
            this.lastTime = this.time();
        } else if (!this.started()) {
            this.lastTime = -1.0;
        }
        // set uniforms
        gl.uniform1f(gl.getUniformLocation(shader, 'u_pointSize'), this.pointSize);
        gl.uniform1f(gl.getUniformLocation(shader, 'u_time'), this.lastTime < 0.0 ? 0.0 : this.time() - this.lastTime);
        gl.uniform1i(gl.getUniformLocation(shader, 'u_started'), this.started());
        gl.uniform3f(gl.getUniformLocation(shader, 'u_offset'), this.offset[0], this.offset[1], this.offset[2]);
        gl.uniform4f(gl.getUniformLocation(shader, 'u_startColor'), this.startColor[0], this.startColor[1], this.startColor[2], this.startColor[3]);
        gl.uniform4f(gl.getUniformLocation(shader, 'u_middleColor'), this.middleColor[0], this.middleColor[1], this.middleColor[2], this.middleColor[3]);
        gl.uniform4f(gl.getUniformLocation(shader, 'u_endColor'), this.endColor[0], this.endColor[1], this.endColor[2], this.endColor[3]);
        gl.uniform1i(gl.getUniformLocation(shader, 'u_repeat'), this.repeat);

        //render elements
        gl.drawArrays(gl.POINTS, 0, this.numItems);
    };
}

/**
 * Custom MaterialSGNode supporting reflectiveness.
 */
class AdvancedMaterialSGNode extends MaterialSGNode {
    constructor(children) {
        super(children);
        this.reflectiveness = 0.0;
    }

    setMaterialUniforms(context) {
        super.setMaterialUniforms(context);

        const gl = context.gl;
        //no materials in use
        if (!context.shader || !isValidUniformLocation(gl.getUniformLocation(context.shader, this.uniform + '.ambient'))) {
            return;
        }
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform + '.reflectiveness'), this.reflectiveness);
    }
}

/**
 * Custom TransformationSGNode for easier animations
 */
class AnimationSGNode extends TransformationSGNode {
    /**
     * Instantiates a new AnimationSGNode with a custom starting position, an animation function and the child nodes.
     * @param initialPosition Initial position of the node.
     * @param animateFunc Animation function of the node. format: (matrixToAnimate) => {...}
     * @param children Children of the node.
     */
    constructor(initialPosition, animateFunc, children) {
        super(mat4.create(), children);
        this.animate = animateFunc || (() => {
        });
        this.initialPosition = initialPosition || mat4.create();
    }

    static rotate(matrix, rotation) {
        if (isNaN(rotation[0]) || isNaN(rotation[1]) || isNaN(rotation[2]))
            return;
        mat4.scalar.rotateX(matrix, matrix, glm.deg2rad(rotation[0] || 0));
        mat4.scalar.rotateY(matrix, matrix, glm.deg2rad(rotation[1] || 0));
        mat4.scalar.rotateZ(matrix, matrix, glm.deg2rad(rotation[2] || 0));
        return matrix;
    }

    static scale(matrix, scale) {
        if (isNaN(scale[0]) || isNaN(scale[1]) || isNaN(scale[2]))
            return;
        mat4.scalar.scale(matrix, matrix, scale || vec3.create());
        return matrix;
    }

    static translate(matrix, translation) {
        if (isNaN(translation[0]) || isNaN(translation[1]) || isNaN(translation[2]))
            return;
        mat4.scalar.translate(matrix, matrix, translation || vec3.create());
        return matrix;
    }

    static bezier2(t, p0, p1) {
        return [
            this.bezier2Single(t, p0[0], p1[0]),
            this.bezier2Single(t, p0[1], p1[1]),
            this.bezier2Single(t, p0[2], p1[2])
        ];
    }

    static bezier3(t, p0, p1, p2) {
        return [
            this.bezier3Single(t, p0[0], p1[0], p2[0]),
            this.bezier3Single(t, p0[1], p1[1], p2[1]),
            this.bezier3Single(t, p0[2], p1[2], p2[2])
        ];
    }

    static bezier2Single(t, p0, p1) {
        // P = (1-t)P1 + tP2
        return (1 - t) * p0 + t * p1;
    }


    static bezier3Single(t, p0, p1, p2) {
        // P = (1−t)^2*P1 + 2(1−t)tP2 + t^2*P3
        return Math.pow(1 - t, 2) * p0 + 2 * (1 - t) * t * p1 + Math.pow(t, 2) * p2;
    }

    static bezier4Single(t, p0, p1, p2, p3) {
        // P = (1−t)^3*P1 + 3(1−t)^2*tP2 +3(1−t)t^2*P3 + t^3*P4
        return Math.pow(1 - t, 3) * p0 + 3 * Math.pow(1 - t, 2) * t * p1 +
            3 * (1 - t) * Math.pow(t, 2) * p2 + Math.pow(t, 3) * p3;
    }


    // https://javascript.info/bezier-curve
    static bezier4(t, p0, p1, p2, p3) {
        return [
            this.bezier4Single(t, p0[0], p1[0], p2[0], p3[0]),
            this.bezier4Single(t, p0[1], p1[1], p2[1], p3[1]),
            this.bezier4Single(t, p0[2], p1[2], p2[2], p3[2])
        ];
    };

    render(context) {
        const mat = mat4.clone(this.initialPosition);
        this.animate(mat);
        this.matrix = mat;
        super.render(context);
    }
}

/**
 * Custom SGNode for handling environment mapping.
 */
class EnvironmentSGNode extends SGNode {
    /**
     * Creates a new EnvironmentSGNode with given textures.
     * @param image_pos_x Right texture of the environment map.
     * @param image_neg_x Left texture of the environment map.
     * @param image_pos_y Top texture of the environment map.
     * @param image_neg_y Bottom texture of the environment map.
     * @param image_pos_z Back texture of the environment map.
     * @param image_neg_z Front texture of the environment map.
     * @param children Children to be rendered within the environment map.
     */
    constructor(image_pos_x,
                image_neg_x,
                image_pos_y,
                image_neg_y,
                image_pos_z,
                image_neg_z,
                children) {
        super(children);
        this.image_pos_x = image_pos_x;
        this.image_neg_x = image_neg_x;
        this.image_pos_y = image_pos_y;
        this.image_neg_y = image_neg_y;
        this.image_pos_z = image_pos_z;
        this.image_neg_z = image_neg_z;
        this.textureunit = 4;
        this.textureId = -1;
        this.enableUniform = 'u_enableSkymap';
    }

    init(gl) {
        this.textureId = gl.createTexture();
        gl.activeTexture(gl.TEXTURE0 + this.textureunit);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.textureId);
        // set parameters for an environment map
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.MIRRORED_REPEAT);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.MIRRORED_REPEAT);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        // set each texture to the corresponding "image" in the cube
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true); // flipping is required for our skybox
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_pos_x);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_X, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_neg_x);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_Y, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_pos_y);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_neg_y);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_Z, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_pos_z);
        gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image_neg_z);
        // unbind the texture again
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    }

    render(context) {
        if (this.textureId < 0) {
            this.init(context.gl);
        }
        //set additional shader parameters
        let invView3x3 = mat3.fromMat4(mat3.create(), context.invViewMatrix); //reduce to 3x3 matrix since we only process direction vectors (ignore translation)
        gl.uniformMatrix3fv(gl.getUniformLocation(context.shader, 'u_invView'), false, invView3x3);
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_texCube'), this.textureunit);
        gl.uniform1i(gl.getUniformLocation(context.shader, this.enableUniform), 1);

        //activate and bind texture
        gl.activeTexture(gl.TEXTURE0 + this.textureunit);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.textureId);

        //render children
        super.render(context);

        gl.uniform1i(gl.getUniformLocation(context.shader, this.enableUniform), 0);

        //clean up
        gl.activeTexture(gl.TEXTURE0 + this.textureunit);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    }
}
//endregion

//region Helper Functions
/**
 * Creates a coordinate system consisting of pyramids with distance 1 for animation (and debug) purposes.
 * @param count Amount of how many cubes should be rendered in each direction (X, Y, Z).
 * @param size Size of cubes to be rendered.
 * @returns {AdvancedMaterialSGNode} Node containing the elements to be rendered.
 */
function createCoordinateSystem(count = [40, 40, 0], size = 0.05) {
    const coordinateNodes = createMaterial({emission: [0, 1, 0, 0.2]});
    let i;
    for (i = 0; i <= count[0]; i++) {
        coordinateNodes.append(
            new TransformationSGNode(
                glm.transform({
                    translate: [i - count[0] / 2, 0, 0],
                    scale: [size, size, size]
                }),
                new PyramidRenderNode()
            )
        )
    }
    for (i = 0; i <= count[1]; i++) {
        coordinateNodes.append(
            new TransformationSGNode(
                glm.transform({
                    translate: [0, 0, i - count[0] / 2],
                    scale: [size, size, size]
                }),
                new PyramidRenderNode()
            )
        )
    }
    for (i = 0; i <= count[2]; i++) {
        coordinateNodes.append(
            new TransformationSGNode(
                glm.transform({
                    translate: [0, i - count[0] / 2, 0],
                    scale: [size, size, size]
                }),
                new PyramidRenderNode()
            )
        )
    }
    return coordinateNodes;
}

/**
 * Helper function to create a spaceship node.
 * @param res Resources object from main program.
 * @param pyramidTexture Texture of the front pyramid to be used.
 * @param injectionNodes Injection points of custom nodes, such as cannonRight.
 * @returns {AdvancedMaterialSGNode} Node containing the element to be rendered.
 */
function createSpaceship(res, pyramidTexture, injectionNodes = {}) {
    return createMaterial({
        ambient: [0.15, 0.15, 0.15, 1.0],
        diffuse: [0.1, 0.1, 0.1, 1.0],
        specular: [0.2, 0.2, 0.2, 1.0],
        shininess: 0.3
    }, new TransformationSGNode(
        glm.scale(0.4, 0.4, 0.4), [
            // front pyramid
            new TextureSGNode(pyramidTexture,
                new TransformationSGNode(
                    glm.transform({
                        translate: [2, 0, 0],
                        rotateZ: -90,
                        rotateX: -90
                    }),
                    new PyramidRenderNode())),
            // front small pyramid
            new TransformationSGNode(
                glm.transform({
                    translate: [1.1, 1.3, 0],
                    scale: [1.0, 0.3, 0.3],
                    rotateZ: -90,
                    rotateX: -90
                }),
                new PyramidRenderNode()),
            // main body
            new TransformationSGNode(
                glm.transform({
                    translate: [-0.5, 0, 0],
                    scale: [1.5, 1, 1]
                }),
                new CubeRenderNode()),
            // right holder
            new TransformationSGNode(
                glm.transform({
                    translate: [0.3, 1.3, 0.9],
                    scale: [0.5, 0.3, 0.1]
                }),
                new CubeRenderNode()),
            // left holder
            new TransformationSGNode(
                glm.transform({
                    translate: [0.3, 1.3, -0.9],
                    scale: [0.5, 0.3, 0.1]
                }),
                new CubeRenderNode()),
            createMaterial({
                    diffuse: [1.00, 0.9, 0.0, 1],
                    emission: [1.0, 0.9, 0.0, 1],
                    shininess: 0.1
                },
                // cannon #1 right
                new TransformationSGNode(
                    glm.transform({
                        translate: [1.5, 0, 0.7],
                        scale: [0.5, 0.2, 0.2]
                    }),
                    [new CubeRenderNode(),
                        injectionNodes.cannonRight || new RenderSGNode(() => {
                        })])),
            createMaterial({
                    diffuse: [1.00, 0.9, 0.0, 1],
                    emission: [1.0, 0.9, 0.0, 1],
                    shininess: 0.1
                },
                // cannon #2 left
                new TransformationSGNode(
                    glm.transform({
                        translate: [1.5, 0, -0.7],
                        scale: [0.5, 0.2, 0.2]
                    }),
                    [new CubeRenderNode(),
                        injectionNodes.cannonLeft || new RenderSGNode(() => {
                        })]),
            ),
            createMaterial({
                diffuse: [1.0, 0, 0.0, 1],
                emission: [1.0, 0.0, 0.0, 0.5],
                specular: [1.0, 0.01, 0.01, 1],
                shininess: 0.1
            }, new AnimationSGNode(undefined, (matrix) => {
                    let t = getTime();
                    // flicker from 15s, and complete turn off from 21s
                    if (Math.random() > 0.4 && t > 15 || t > 20) {
                        // move into spaceship to hide
                        AnimationSGNode.translate(matrix, [2, 0, 0]);
                    }
                }, [
                    // engine #1 left
                    new TransformationSGNode(
                        glm.transform({
                            translate: [-2, 0, -0.7],
                            scale: [0.1, 0.8, 0.2]
                        }),
                        new CubeRenderNode()),
                    // engine #2 middle
                    new TransformationSGNode(
                        glm.transform({
                            translate: [-2, 0, 0],
                            scale: [0.1, 0.8, 0.2]
                        }),
                        new CubeRenderNode()),
                    // engine #3 right
                    new TransformationSGNode(
                        glm.transform({
                            translate: [-2, 0, 0.7],
                            scale: [0.1, 0.8, 0.2]
                        }),
                        new CubeRenderNode())
                ])),
            // alien
            createMaterial({
                diffuse: [0.00, 1.00, 0.10, 1],
                specular: [0.37, 0.37, 0.37, 1],
                emission: [0.00, 0.81, 0.24, 1],
                shininess: 0.4
            }, [
                // alien body
                new TransformationSGNode(
                    glm.transform({
                        translate: [0, 1.1, 0],
                        scale: [0.1, 0.5, 0.1]
                    }),
                    new CubeRenderNode()),
                // alien left hand
                new TransformationSGNode(
                    glm.transform({
                        translate: [0.4, 1.5, -0.3],
                        scale: [0.5, 0.1, 0.1],
                        rotateZ: 30,
                        rotateY: 30
                    }),
                    new CubeRenderNode()),
                // alien right hand
                new TransformationSGNode(
                    glm.transform({
                        translate: [0.4, 1.5, 0.3],
                        scale: [0.5, 0.1, 0.1],
                        rotateZ: 30,
                        rotateY: -30
                    }),
                    new CubeRenderNode()),
                // alien head
                new AlphaSGNode(res.cracks,
                    new TransformationSGNode(
                        glm.translate(0, 2, 0),
                        new RenderSGNode(makeSphere(0.3, 20, 20),
                            injectionNodes.head || new RenderSGNode(() => {
                            })))),
            ]),
            injectionNodes.general || new RenderSGNode(() => {
            }),
        ]))
        ;
}

/**
 * Creates a new material node with given properties.
 * @param ambient Ambient property of the material.
 * @param diffuse Diffuse property of the material.
 * @param specular Specular property of the material.
 * @param emission Emission property of the material.
 * @param shininess Shininess property of the material.
 * @param reflectiveness Reflectiveness property of the material.
 * @param children Children of the node.
 * @returns {AdvancedMaterialSGNode} Material node with the given properties.
 */
function createMaterial({
                            ambient = [0, 0, 0, 1],
                            diffuse = [0, 0, 0, 1],
                            specular = [0, 0, 0, 1],
                            emission = [0, 0, 0, 1],
                            shininess = 0.0,
                            reflectiveness = 0.0
                        }, children) {
    let materialNode = new AdvancedMaterialSGNode(children);
    materialNode.ambient = ambient;
    materialNode.diffuse = diffuse;
    materialNode.specular = specular;
    materialNode.emission = emission;
    materialNode.shininess = shininess;
    materialNode.reflectiveness = reflectiveness;
    return materialNode;
}

/**
 * Creates a new light node with given properties.
 * @param ambient Ambient property of the light.
 * @param diffuse Diffuse property of the light.
 * @param specular Specular property of the light.
 * @param uniform Uniform (name) of the light.
 * @param position Position of the light.
 * @param children Children of the node.
 * @returns {AdvancedLightSGNode} Light node with the given properties.
 */
function createLight({
                         ambient = [0, 0, 0, 1],
                         diffuse = [1, 1, 1, 1],
                         specular = [1, 1, 1, 1]
                     }, uniform, position, children) {
    let lightNode = new AdvancedLightSGNode(position, children);
    lightNode.uniform = uniform;
    lightNode.ambient = ambient;
    lightNode.diffuse = diffuse;
    lightNode.specular = specular;
    return lightNode;
}

/**
 * Creates a new spotlight node with given properties.
 * @param ambient Ambient property of the light.
 * @param diffuse Diffuse property of the light.
 * @param specular Specular property of the light.
 * @param uniform Uniform (name) of the light.
 * @param position Position of the light.
 * @param direction Direction of the light.
 * @param limit Limit (in degrees) of the light.
 * @param children Children of the node.
 * @returns {SpotlightSGNode} Spotlight node with the given properties.
 */
function createSpotlight({
                             ambient = [0, 0, 0, 1],
                             diffuse = [1, 1, 1, 1],
                             specular = [1, 1, 1, 1]
                         }, uniform, position, direction, limit, children) {
    let lightNode = new SpotlightSGNode(position, direction, limit, children);
    lightNode.uniform = uniform;
    lightNode.ambient = ambient;
    lightNode.diffuse = diffuse;
    lightNode.specular = specular;
    return lightNode;
}
//endregion

//region Control Classes
/**
 * Control to react to mouse movements.
 */
class MouseControl {
    /**
     * Creates a MouseControl with given parameters.
     * @param canvas Canvas on which the mouse should listen.
     */
    constructor(canvas) {
        this.canvas = canvas;
        this.clicked = false;
        this.speed = 0.5;
        this.previousX = 0;
        this.previousY = 0;
        this.y = 0;
        this.x = 0;

        this.mouseUpListener = () => {
            this.clicked = false;
        };
        this.mouseMoveListener = (e) => {
            // return if mouse is not clicked
            if (!this.clicked) return false;
            // otherwise, calculate delta from previous position
            let deltaX = (e.pageX - this.previousX) * 2 * Math.PI / canvas.width;
            let deltaY = (e.pageY - this.previousY) * 2 * Math.PI / canvas.height;
            // multiply by speed constant
            deltaX *= this.speed;
            deltaY *= this.speed;
            // add delta to our position
            this.x += deltaX;
            this.y += deltaY;
            // and save current as previous position
            this.previousX = e.pageX;
            this.previousY = e.pageY;
            // stop default handling
            e.preventDefault();
        };

        this.mouseDownListener = (e) => {
            this.clicked = true;
            this.previousX = e.pageX;
            this.previousY = e.pageY;
            e.preventDefault();
            return false;
        };
    }

    start() {
        this.canvas.addEventListener("mousedown", this.mouseDownListener, false);
        this.canvas.addEventListener("mouseup", this.mouseUpListener, false);
        this.canvas.addEventListener("mouseout", this.mouseUpListener, false);
        this.canvas.addEventListener("mousemove", this.mouseMoveListener, false);
    }

    stop() {
        this.canvas.removeEventListener("mousedown", this.mouseDownListener);
        this.canvas.removeEventListener("mouseup", this.mouseUpListener);
        this.canvas.removeEventListener("mouseout", this.mouseUpListener);
        this.canvas.removeEventListener("mousemove", this.mouseMoveListener);
    }
}

/**
 * Control to react to key presses.
 */
class KeyboardControl {
    /**
     * Creates a KeyboardControl with given parameters.
     * @param callback Function to be called when a button is pressed.
     * First parameter is keycode of the pressed key.
     */
    constructor(callback) {
        this.pressedKeys = {};
        this.callback = callback;
        // check keycodes at https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
        this.keyDownListener = (e) => {
            this.pressedKeys[e.keyCode] = true;
            if (this.callback) {
                this.callback(e.keyCode);
            }
        };
        this.keyUpListener = (e) => {
            this.pressedKeys[e.keyCode] = false;
        };
    }

    a() {
        return this.pressedKeys[65];
    }

    d() {
        return this.pressedKeys[68];
    }

    w() {
        return this.pressedKeys[87];
    }

    s() {
        return this.pressedKeys[83];
    }

    r() {
        return this.pressedKeys[82];
    }

    f() {
        return this.pressedKeys[70];
    }

    start() {
        document.addEventListener("keydown", this.keyDownListener, false);
        document.addEventListener("keyup", this.keyUpListener, false);
    }

    stop() {
        document.removeEventListener("keydown", this.keyDownListener);
        document.removeEventListener("keyup", this.keyUpListener);
    }
}
//endregion